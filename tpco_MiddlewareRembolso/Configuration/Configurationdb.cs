﻿using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace tpco_MiddlewareRembolso.Configuration
{
    public class Configurationdb
    {
        public readonly IConfiguration configuration;
        public string ConnectionGet()
        {
            var connectionString = AppSettingsC.Instance.GetConnection("ConnectionString");
            return connectionString;
          
        }
    }
}
