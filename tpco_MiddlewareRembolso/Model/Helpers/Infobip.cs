﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Web;

namespace TPCO.Middleware.Core.Common
{
    public class Infobip
    {
        public WebRequest req;
        public string postData;
        public ASCIIEncoding encoding;
        public byte[] byte1;
        public Stream newStream;
        public HttpWebResponse WebResponse;

        public InfobipResponse responseInfobip;
        public InfobipRequest requestInfobip;

        public Byte[] plainTextBytes;
        public string encodedText;
        public string responseText;
        public string mRequestInfobip;

        public string urlInfobip;
        public string indicative;
        public string usernameInfobip;
        public string passwordInfobip;

        public message _message;
        public status _Status;

        public InfobipResponse SendSMSInfobip(string CellPhone, string Message)
        {
            responseInfobip = new InfobipResponse();
            _message = new message();
            _Status = new status();

            try
            {
                plainTextBytes = Encoding.UTF8.GetBytes(usernameInfobip + ":" + passwordInfobip);
                encodedText = Convert.ToBase64String(plainTextBytes);

                requestInfobip = new InfobipRequest();
                requestInfobip.from = "SYSTEM";
                requestInfobip.to = indicative + CellPhone;
                requestInfobip.text = Message;
                mRequestInfobip = JsonConvert.SerializeObject(requestInfobip);

                req = WebRequest.Create(@"" + urlInfobip + "");
                req.Method = "POST";
                req.Headers.Add("Authorization: " + "Basic " + encodedText);
                req.ContentType = "application/json";

                postData = mRequestInfobip;
                encoding = new ASCIIEncoding();
                //byte1 = encoding.GetBytes(postData);
                byte1 = UTF8Encoding.UTF8.GetBytes(postData);
                req.ContentLength = byte1.Length;
                newStream = req.GetRequestStream();
                newStream.Write(byte1, 0, byte1.Length);

                WebResponse = req.GetResponse() as HttpWebResponse;

                responseInfobip = new InfobipResponse();

                using (var reader = new System.IO.StreamReader(WebResponse.GetResponseStream(), encoding))
                {
                    responseText = reader.ReadToEnd();
                    responseInfobip = JsonConvert.DeserializeObject<InfobipResponse>(responseText);
                }

                WebResponse.Close();
            }
            catch (System.Net.WebException ex)
            {
                if (ex.Response != null)
                {
                    Stream responseStream = ex.Response.GetResponseStream();
                    StreamReader Reader = new StreamReader(responseStream, Encoding.Default);
                    string response = Reader.ReadToEnd();
                    responseStream.Close();
                    _Status.groupName = "WebException";
                    _Status.description = response;
                    _message.status = _Status;
                    responseInfobip.messages.Add(_message);
                }
            }
            catch (Exception ex)
            {
                _Status.groupName = "Exception";
                _Status.description = ex.Message;
                _message.status = _Status;
                responseInfobip.messages.Add(_message);
            }
            return responseInfobip;
        }
    }

    public class InfobipRequest
    {
        public string from { get; set; }
        public string to { get; set; }
        public string text { get; set; }
    }

    public class InfobipResponse
    {
        public List<message> messages { get; set; }
    }

    public class status
    {
        public string groupId { get; set; }
        public string groupName { get; set; }
        public string id { get; set; }
        public string name { get; set; }
        public string description { get; set; }
    }

    public class message
    {
        public string to { get; set; }
        public status status { get; set; }
        public string smsCount { get; set; }
        public string messageId { get; set; }
    }

}
