﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace tpco_MiddlewareRembolso.Model.Helpers
{
    public class SendSMS
    {
        [Required]
        public string Number { get; set; }
        [Required]
        public string Message { get; set; }
    }
}
