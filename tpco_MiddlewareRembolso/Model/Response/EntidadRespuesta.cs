﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace tpco_MiddlewareRembolso.Model.Response
{
    public class EntidadRespuesta
    {
        public string codigoRespuesta { get; set; }
        public string descripcionRespuesta { get; set; }
    }
}
