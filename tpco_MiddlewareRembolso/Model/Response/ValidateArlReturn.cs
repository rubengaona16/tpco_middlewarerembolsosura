﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace tpco_MiddlewareRembolso.Model.Response
{
    public class ValidateArlReturn: EntidadRespuesta
    {
        public string TipoDocumento { get; set; }
        public int CatidadDocuments { get; set; }
    }
}
