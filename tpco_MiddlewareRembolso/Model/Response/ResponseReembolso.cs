﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace tpco_MiddlewareRembolso.Model.Response
{
    public class ResponseReembolso
    {

        public Partnerdata partnerData { get; set; }

        public class Partnerdata
        {
            public Controldata controlData { get; set; }
            public string document { get; set; }
            public string documentType { get; set; }
            public string lastName { get; set; }
            public Listdocument[] listDocuments { get; set; }
            public string name { get; set; }
        }

        public class Controldata
        {
            public int pageNumber { get; set; }
            public int pages { get; set; }
            public int regPerPage { get; set; }
            public float totalAmount { get; set; }
            public int totalRegistries { get; set; }
        }

        public class Listdocument
        {
            public string accountType { get; set; }
            public Additionalinfo additionalInfo { get; set; }
            public Alternativepayee alternativePayee { get; set; }
            public Amount amount { get; set; }
            public Amountdoc amountDoc { get; set; }
            public string bankAccount { get; set; }
            public string bankAccountType { get; set; }
            public string baselineDate { get; set; }
            public string businessArea { get; set; }
            public string certCode { get; set; }
            public string clearingDate { get; set; }
            public string clrngDocument { get; set; }
            public string companyCode { get; set; }
            public string debitCredit { get; set; }
            public string documentClass { get; set; }
            public string documentDate { get; set; }
            public string documentNumber { get; set; }
            public string dsCompany { get; set; }
            public string dsReference2 { get; set; }
            public string exchangeRate { get; set; }
            public string exRatePolicy { get; set; }
            public string houseBank { get; set; }
            public string item { get; set; }
            public string lockCode { get; set; }
            public string lockDescription { get; set; }
            public string partBankType { get; set; }
            public string paymentForm { get; set; }
            public string posText { get; set; }
            public string postingDate { get; set; }
            public string process { get; set; }
            public string pymtMethod { get; set; }
            public string reference { get; set; }
            public string reference1 { get; set; }
            public string reference2 { get; set; }
            public string reference3 { get; set; }
            public string referenceKey { get; set; }
            public string statusGroup { get; set; }
            public string time { get; set; }
            public string year { get; set; }
        }

        public class Additionalinfo
        {
            public string avsid { get; set; }
            public string bank { get; set; }
            public string bankAccount { get; set; }
            public string bankCountry { get; set; }
            public string bankName { get; set; }
            public string paymentCode { get; set; }
            public string paymentInfo { get; set; }
            public string paymentText { get; set; }
            public string paymentType { get; set; }
        }

        public class Alternativepayee
        {
            public string document { get; set; }
            public string documentType { get; set; }
            public string lastName { get; set; }
            public string name { get; set; }
        }

        public class Amount
        {
            public string currency { get; set; }
            public float total { get; set; }
        }

        public class Amountdoc
        {
            public string currency { get; set; }
            public float total { get; set; }
        }

    }
}
