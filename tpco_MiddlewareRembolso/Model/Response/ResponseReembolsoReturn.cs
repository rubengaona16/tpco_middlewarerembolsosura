﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace tpco_MiddlewareRembolso.Model.Response
{
    public class ResponseReembolsoReturn: EntidadRespuesta
    {
        public string paymentInfo { get; set; }
        public string clearingDate { get; set; }
        public string bankAccount { get; set; }
        public string bankName { get; set; }
        public string pymetMethod { get; set; }
        
    }
    

}