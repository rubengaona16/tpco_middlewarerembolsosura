﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace tpco_MiddlewareRembolso.Model.Response
{
    public class ResponseValidationARL
    {

		[XmlRoot(ElementName = "TIPO_AFILIADO")]
		public class TIPOAFILIADO
		{

			[XmlElement(ElementName = "TIPO_ID")]
			public string TIPOID { get; set; }

			[XmlElement(ElementName = "NUMERO")]
			public int NUMERO { get; set; }

			[XmlElement(ElementName = "EMPRESA")]
			public string EMPRESA { get; set; }

			[XmlElement(ElementName = "SNAFI_EMPRESA")]
			public string SNAFIEMPRESA { get; set; }

			[XmlElement(ElementName = "DEPENDIENTE")]
			public string DEPENDIENTE { get; set; }

			[XmlElement(ElementName = "SNAFI_DEPENDIENTE")]
			public string SNAFIDEPENDIENTE { get; set; }

			[XmlElement(ElementName = "INDEPENDIENTE")]
			public string INDEPENDIENTE { get; set; }

			[XmlElement(ElementName = "SNAFI_INDEPENDIENTE")]
			public string SNAFIINDEPENDIENTE { get; set; }

			[XmlElement(ElementName = "ESTUDIANTE")]
			public string ESTUDIANTE { get; set; }

			[XmlElement(ElementName = "SNAFI_ESTUDIANTE")]
			public string SNAFIESTUDIANTE { get; set; }

			[XmlElement(ElementName = "VOLUNTARIO")]
			public string VOLUNTARIO { get; set; }

			[XmlElement(ElementName = "SNAFI_VOLUNTARIO")]
			public string SNAFIVOLUNTARIO { get; set; }

			[XmlElement(ElementName = "PENSIONADO")]
			public string PENSIONADO { get; set; }

			[XmlElement(ElementName = "SNAFI_PENSIONADO")]
			public string SNAFIPENSIONADO { get; set; }
		}

		[XmlRoot(ElementName = "PARAMETROS")]
		public class PARAMETROS
		{

			[XmlElement(ElementName = "TIPO_AFILIADO")]
			public TIPOAFILIADO TIPOAFILIADO { get; set; }

			[XmlElement(ElementName = "RESULTADO")]
			public int RESULTADO { get; set; }
		}


	}
}