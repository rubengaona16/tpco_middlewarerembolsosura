﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace tpco_MiddlewareRembolso.Model
{
	public class Log
	{
		public DateTime Logtime { get; set; }
		public string Action { get; set; }
		public string Message { get; set; }
		public bool result { get; set; }
		public string Username { get; set; } //r:read, w:write
	}
	public class Logresponse
	{
		public string Result { get; set; }
	}
}
