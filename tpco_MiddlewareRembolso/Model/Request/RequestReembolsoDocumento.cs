﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace tpco_MiddlewareRembolso.Model.Request
{
    public class RequestReembolsoDocumento
    {
        public string Document { get; set; }
        public string TypeDocument { get; set; }
        public string V_Speed { get; set; }
        public string X_Request_ID { get; set; }
    }
}
