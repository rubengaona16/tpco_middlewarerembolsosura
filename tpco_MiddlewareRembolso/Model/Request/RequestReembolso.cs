﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace tpco_MiddlewareRembolso.Model.Request
{
    public class RequestReembolso
    {
        public class ReembolsoObject
        {
            public Controldata controlData { get; set; }
            public Filters filters { get; set; }
        }

        public class Controldata
        {
            public string cdFuente { get; set; }
            public string certCode { get; set; }
            public string companyCode { get; set; }
            public string process { get; set; }
            public string accountType { get; set; }
            public string noDocuments { get; set; }
            public int pageNumber { get; set; }
            public int regPerPage { get; set; }
        }

        public class Filters
        {
            public string document { get; set; }
            public string documentType { get; set; }
            public string status { get; set; }
            public string reference { get; set; }
            public string referenceKey { get; set; }
            public object glFilters { get; set; }
            public object arFilters { get; set; }
            public Apfilters apFilters { get; set; }
        }

        public class Apfilters
        {
            public string documentNumber { get; set; }
            public string initialDate { get; set; }
            public string finalDate { get; set; }
            public string snPaymentInfo { get; set; }
            public string snClearedDocs { get; set; }
        }

    }
}
