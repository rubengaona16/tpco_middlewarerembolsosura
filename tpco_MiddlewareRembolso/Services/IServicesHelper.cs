﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using tpco_MiddlewareRembolso.Model.Helpers;
using tpco_MiddlewareRembolso.Model.Response;

namespace tpco_MiddlewareRembolso.Services
{
    public interface IServicesHelper
    {
        ResponseSMS EnviarSMS(SendSMS parametros);
    }
}
