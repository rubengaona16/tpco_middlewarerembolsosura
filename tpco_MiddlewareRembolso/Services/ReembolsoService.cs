﻿using Newtonsoft.Json;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using tpco_MiddlewareRembolso.Configuration;
using tpco_MiddlewareRembolso.Model.Helpers;
using tpco_MiddlewareRembolso.Model.Request;
using tpco_MiddlewareRembolso.Model.Response;

namespace tpco_MiddlewareRembolso.Services
{
    public class ReembolsoService : IReembolsoService
    {

        public List<ResponseReembolsoReturn> requestReembolso(RequestReembolsoDocumento requestReembolso, string v)
        {

            try
            {
                var responseReembolsoReturn = new List<ResponseReembolsoReturn>();
                var body = "";
                if (v == "L")
                {
                    var JsonObject = JconverterObject(requestReembolso, "L");
                    body = JsonObject;
                }
                else if (v == "P")
                {
                    var JsonObject = JconverterObject(requestReembolso, "P");
                    body = JsonObject;
                }
                var URL = AppSettingsC.Instance.GetString("URL");
                var VFuente = AppSettingsC.Instance.GetString("cdFuente");
                var MonthReturn = AppSettingsC.Instance.GetInt32("MonthReturn");
                var responseReembolso = new ResponseReembolso();
                var client = new RestClient(URL);
                client.Timeout = -1;
                var request = new RestRequest(Method.POST);
                request.AddHeader("V-Fuente", VFuente);
                request.AddHeader("V-Seed", "2021-08-13:15:19:01");//reemplazar
                request.AddHeader("X-Request-ID", "YC0qXZn4cva0hUYtKmok1naGvm4=");//reemplazar
                request.AddHeader("Content-Type", "application/json");
                request.AddParameter("application/json", body, ParameterType.RequestBody);


                IRestResponse response = client.Execute(request);
                if (response.StatusCode == System.Net.HttpStatusCode.OK)
                {
                    responseReembolso = JsonConvert.DeserializeObject<ResponseReembolso>(response.Content);
                    responseReembolsoReturn = (from item in responseReembolso.partnerData.listDocuments
                                               where Convert.ToDateTime(item.clearingDate == "0000-00-00" ? DateTime.Now : item.clearingDate) >= DateTime.Today.AddMonths(MonthReturn)
                                               select new ResponseReembolsoReturn()
                                               {
                                                   paymentInfo=item.additionalInfo.paymentInfo,
                                                   bankAccount = item.additionalInfo.bankAccount,
                                                   clearingDate = item.clearingDate,
                                                   bankName = item.additionalInfo.bankName,
                                                   pymetMethod = item.pymtMethod,
                                                   codigoRespuesta="1",
                                                   descripcionRespuesta = "Consumo con Exito"

                                               }).ToList();

                    return responseReembolsoReturn;
                }
                return null;
            }
            catch (Exception ex)
            {
                return null;
            }
        }



        public string JconverterObject(RequestReembolsoDocumento requestReembolso, string v)
        {
            try
            {
                var filterDate = new RequestReembolso.Apfilters();
                var result = new RequestReembolso.ReembolsoObject();
                result.controlData = new RequestReembolso.Controldata();
                result.filters = new RequestReembolso.Filters();
                var EndDate = DateTime.Today;
                var DateCurrent = EndDate.AddMonths(-3);
                var EndDateConvert= EndDate.ToString("yyyy-MM-dd");
                var DateCurrentConvert= DateCurrent.ToString("yyyy-MM-dd");
                if (v == "L")
                {

                    filterDate.initialDate = DateCurrentConvert;
                    filterDate.finalDate = EndDateConvert;
                    filterDate.documentNumber = "";
                    filterDate.snClearedDocs = "";
                    filterDate.snPaymentInfo = "";
                    result.controlData.cdFuente = AppSettingsC.Instance.GetString("cdFuente");
                    result.controlData.certCode = AppSettingsC.Instance.GetString("certCode");
                    result.controlData.companyCode = AppSettingsC.Instance.GetString("companyCode");
                    result.controlData.process = AppSettingsC.Instance.GetString("process");
                    result.controlData.accountType = AppSettingsC.Instance.GetString("accountType");
                    result.controlData.noDocuments = AppSettingsC.Instance.GetString("noDocuments");
                    result.controlData.pageNumber = Convert.ToInt32(AppSettingsC.Instance.GetString("pageNumber"));
                    result.controlData.regPerPage = Convert.ToInt32(AppSettingsC.Instance.GetString("regPerPage"));
                    result.filters.document = requestReembolso.Document;
                    result.filters.documentType = requestReembolso.TypeDocument;
                    result.filters.status = AppSettingsC.Instance.GetString("status");
                    result.filters.reference = AppSettingsC.Instance.GetString("reference");
                    result.filters.referenceKey = AppSettingsC.Instance.GetString("referenceKey");
                    result.filters.glFilters = null;
                    result.filters.arFilters = null;
                    result.filters.apFilters = filterDate;
                }
                else if (v == "P")
                {
                    result.controlData.cdFuente = AppSettingsC.Instance.GetString("cdFuente");
                    result.filters.document = requestReembolso.Document;
                    result.filters.documentType = requestReembolso.TypeDocument;
                }

                var Jresult = JsonConvert.SerializeObject(result);
                return Jresult;
            }
            catch (Exception ex)
            {
                return null;
            }
        }
    }
}
