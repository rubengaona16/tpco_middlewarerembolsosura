﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using tpco_MiddlewareRembolso.Model.Request;
using tpco_MiddlewareRembolso.Model.Response;

namespace tpco_MiddlewareRembolso.Services
{
    public interface IReembolsoService
    {
        List<ResponseReembolsoReturn> requestReembolso(RequestReembolsoDocumento requestReembolso,string v);
      
    }
}

