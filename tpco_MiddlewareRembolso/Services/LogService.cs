﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TP_Connection.Data.SqlServer;
using tpco_MiddlewareRembolso.Configuration;
using tpco_MiddlewareRembolso.Model;

namespace tpco_MiddlewareRembolso.Services
{
    public class LogService:ILogService
    {
        private Configurationdb configDb;
        public LogService()
        {
            configDb = new Configurationdb();
        }

        public int logAdd(List<Log> log)
        {
            int bResult = 0;            
            DbBase db_ = new DbBase(configDb.ConnectionGet());

 

            foreach (Log lrow in log)
            {
         
                DateTime utcTime = DateTime.UtcNow;
                TimeZoneInfo myZone = TimeZoneInfo.CreateCustomTimeZone("COLOMBIA", new TimeSpan(-5, 0, 0), "Colombia", "Colombia");
                DateTime custDateTime = TimeZoneInfo.ConvertTimeFromUtc(utcTime, myZone);

                lrow.Logtime = custDateTime;
                List<Logresponse> result = db_.SearchList<Logresponse>(lrow, "spLogAdd");

                if (result[0].Result.Equals("1"))
                {
                    bResult++;
                }
            }

            return bResult;
        }
    }
}
