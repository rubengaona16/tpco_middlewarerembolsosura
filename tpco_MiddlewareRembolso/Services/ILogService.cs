﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using tpco_MiddlewareRembolso.Model;

namespace tpco_MiddlewareRembolso.Services
{
    public interface ILogService
    {
        int logAdd(List<Log> log);
    }
}
