﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TPCO.Middleware.Core.Common;
using tpco_MiddlewareRembolso.Configuration;
using tpco_MiddlewareRembolso.Model.Helpers;
using tpco_MiddlewareRembolso.Model.Response;

namespace tpco_MiddlewareRembolso.Services
{
    public class ServicesHelper : IServicesHelper
    {
        public ResponseSMS EnviarSMS(SendSMS parametros)
        {
           
            string LastError = string.Empty;

            string Method = "SendSMSInfobip";

            var response = new ResponseSMS();

            Infobip objSMS = new Infobip();
            InfobipResponse objSMRResponse = new InfobipResponse();
            var watch = new System.Diagnostics.Stopwatch();

            
            objSMS.usernameInfobip = AppSettingsC.Instance.GetString("usuarioInfobip");
            objSMS.passwordInfobip = AppSettingsC.Instance.GetString("claveInfobip");
            objSMS.urlInfobip = AppSettingsC.Instance.GetString("urlAPIInfobip");
            objSMS.indicative = AppSettingsC.Instance.GetString("indicativocelularSMS");
            objSMS._message = new message();
            objSMS._Status = new status();

            watch.Start();
            objSMRResponse = objSMS.SendSMSInfobip(parametros.Number, parametros.Message);
            watch.Stop();

            response.ResponseInfobip = objSMRResponse;

            if (objSMRResponse.messages[0].status.groupName.ToUpper() == "PENDING")
            {                
                response.codigoRespuesta = "1";
                response.descripcionRespuesta = string.Format("{0} - {1}", objSMRResponse.messages[0].status.groupName, objSMRResponse.messages[0].status.description);
            }
            else
            {                
                response.codigoRespuesta = "-999";
                response.descripcionRespuesta = string.Format("{0} - {1}", objSMRResponse.messages[0].status.groupName, objSMRResponse.messages[0].status.description);
            }

            
            objSMRResponse = null;

            return response;
        }
    }
}
