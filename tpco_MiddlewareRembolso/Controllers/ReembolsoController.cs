﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using tpco_MiddlewareRembolso.Model;
using tpco_MiddlewareRembolso.Model.Helpers;
using tpco_MiddlewareRembolso.Model.Request;
using tpco_MiddlewareRembolso.Model.Response;
using tpco_MiddlewareRembolso.Services;

namespace tpco_MiddlewareRembolso.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class ReembolsoController : ControllerBase
    {
        #region fields
        private LogService log;
        private List<Log> llog;
        private readonly IReembolsoService _reembolsoService;
        private readonly IServicesHelper _serviceHelper;
        #endregion

        #region ctor

        public ReembolsoController(IReembolsoService reembolsoService, IServicesHelper servicesHelper)
        {
            _reembolsoService = reembolsoService;
            _serviceHelper = servicesHelper;
            log = new LogService();
            llog = new List<Log>();
        }

        #endregion

        #region methods

        [HttpPost]
        public IActionResult ValidationReembolso([FromBody] RequestReembolsoDocumento user)
        {

            var respuesta = new EntidadRespuesta();
            try
            {
                var userResponse = _reembolsoService.requestReembolso(user, "L");

                if (userResponse.Count == 0)
                {
                    var message = "Usuario no encontrado";
                    llog.Add(new Log { Logtime = DateTime.Now, Action = "ValidationReembolso", Message = message, Username = "", });
                    log.logAdd(llog);
                    respuesta.codigoRespuesta = "-1";
                    respuesta.descripcionRespuesta = message;
                    return BadRequest(respuesta);
                }

                return Ok(userResponse);
            }
            catch (Exception ex)
            {
                llog.Add(new Log { Logtime = DateTime.Now, Action = "ValidationReembolso", Message = ex.Message, Username = "", });
                log.logAdd(llog);
                respuesta.codigoRespuesta = "-1";
                respuesta.descripcionRespuesta = ex.Message;
                return BadRequest(respuesta);
            }


        }

        [HttpPost]
        [Route("Pendiente")]
        public IActionResult ValidationReembolsoP([FromBody] RequestReembolsoDocumento user)
        {
            var message = "Usuario no encontrado";
            var userResponse = new List<ResponseReembolsoReturn>();
            var respuesta = new EntidadRespuesta();
            try
            {
                userResponse = _reembolsoService.requestReembolso(user, "P");
                if (userResponse.Count == 0)
                {
                    llog.Add(new Log { Logtime = DateTime.Now, Action = "ValidationReembolsoP", Message = message, Username = "", });
                    log.logAdd(llog);
                    respuesta.codigoRespuesta = "-1";
                    respuesta.descripcionRespuesta = message;
                    return BadRequest(respuesta);
                }
                llog.Add(new Log { Logtime = DateTime.Now, Action = "ValidationReembolsoP", Message = "Consumo exitoso", Username = "", });
                log.logAdd(llog);
                return Ok(userResponse);
            }
            catch (Exception ex)
            {
                llog.Add(new Log { Logtime = DateTime.Now, Action = "ValidationReembolsoP", Message = ex.Message, Username = "", });
                log.logAdd(llog);
                respuesta.codigoRespuesta = "-1";
                respuesta.descripcionRespuesta = ex.Message;
                return BadRequest(respuesta);
            }


        }

        [Route("EnviarSMS")]
        [HttpPost]
        //[Authorize]
        public ResponseSMS SendSMS([FromBody] SendSMS parameters)
        {
            var ResponseSendSMS = new ResponseSMS();
            try
            {
                ResponseSendSMS = _serviceHelper.EnviarSMS(parameters);
                llog.Add(new Log { Logtime = DateTime.Now, Action = "SendSMS", Message = "Se envio sms", Username = "", });
                log.logAdd(llog);
                return ResponseSendSMS;
            }
            catch (Exception ex)
            {
                llog.Add(new Log { Logtime = DateTime.Now, Action = "ValidationReembolsoP", Message = ex.Message, Username = "", });
                log.logAdd(llog);
                ResponseSendSMS.codigoRespuesta = "-1";
                ResponseSendSMS.descripcionRespuesta = ex.Message;
                return ResponseSendSMS;
            }

        }
        #endregion



    }
}
