﻿using Microsoft.AspNetCore.Mvc;
//using ServiceReference1;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using tpco_MiddlewareRembolso.Model.Request;
using tpco_MiddlewareRembolso.Services;
using AudioRespuestaWS;
using System.Xml.Serialization;
using System.IO;
using tpco_MiddlewareRembolso.Model.Response;
using System.Xml;
using static tpco_MiddlewareRembolso.Model.Response.ResponseValidationARL;
using tpco_MiddlewareRembolso.Model;

namespace tpco_MiddlewareRembolso.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class ValidarClienteController : ControllerBase
    {

        #region fields
        private LogService log;
        private List<Log> llog;


        #endregion

        #region ctor

        public ValidarClienteController()
        {

            log = new LogService();
            llog = new List<Log>();
        }

        #endregion

        #region Methods

        [HttpPost]
        public async Task<IActionResult> ValidationARL([FromBody] RequestValidationARL user)
        {
            var validateArlReturn = new ValidateArlReturn();
            try
            {
                var xmlClass = new PARAMETROS();
                var audioRespuesta = new AudioRespuestaClient(new AudioRespuestaClient.EndpointConfiguration());
                var responseWS = await audioRespuesta.validarClienteARLAsync(user.documentoIdentidadUsuario);
                if (responseWS != null)
                {
                    var serializer = new XmlSerializer(typeof(PARAMETROS));
                    using (StringReader reader = new StringReader(responseWS))
                    {
                        xmlClass = (PARAMETROS)serializer.Deserialize(reader);
                    }
                    validateArlReturn.TipoDocumento = xmlClass.TIPOAFILIADO.TIPOID;
                    validateArlReturn.CatidadDocuments = xmlClass.RESULTADO;
                    validateArlReturn.codigoRespuesta = "1";
                    validateArlReturn.descripcionRespuesta = "se consumio con exito";                    
                    llog.Add(new Log { Logtime = DateTime.Now, Action = "ValidationARL", Message = "Consumo exitoso", Username = "", });
                    log.logAdd(llog);
                    return Ok(validateArlReturn);
                }
                else
                {
                    llog.Add(new Log { Logtime = DateTime.Now, Action = "ValidationARL", Message = "No se encontro ningun dato de este numero de documento", Username = "", });
                    log.logAdd(llog);
                    validateArlReturn.codigoRespuesta = "-1";
                    validateArlReturn.descripcionRespuesta = "No se encontro ningun dato de este numero de documento";
                    return BadRequest(validateArlReturn);
                }

            }
            catch (Exception ex)
            {
                llog.Add(new Log { Logtime = DateTime.Now, Action = "ValidationARL", Message = ex.Message, Username = "", });
                log.logAdd(llog);
                validateArlReturn.codigoRespuesta = "-1";
                validateArlReturn.descripcionRespuesta = ex.Message;
                return BadRequest(validateArlReturn);
            }


        }

        #endregion

    }
}
